package util

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

func GetUserInfo(toUrl string, headerToken string) []byte {

	request, _ := http.NewRequest("GET", toUrl, nil)
	request.Header.Add("token", headerToken)
	client := new(http.Client)
	response, err := client.Do(request)
	if err != nil {

		return nil
	}

	defer response.Body.Close()

	if response.StatusCode == 200 {
		byteArr, err := ioutil.ReadAll(response.Body)
		if err != nil {

			return nil
		}
		jsonStr := string(byteArr)

		if len(jsonStr) > 2 {
			return byteArr
		}
	}

	return nil
}

func Get(toUrl string) []byte {

	request, _ := http.NewRequest("GET", toUrl, nil)
	client := new(http.Client)

	response, err := client.Do(request)
	if err != nil {
		return nil
	}

	defer response.Body.Close()

	if response.StatusCode == 200 {
		byteArr, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return nil
		}
		jsonStr := string(byteArr)

		if len(jsonStr) > 2 {
			return byteArr
		}
	}
	return nil
}

func Post(toUrl string, param url.Values) ([]byte, string) {

	client := new(http.Client)
	resp, err := client.PostForm(toUrl, param)

	if err != nil {
		return nil, ""
	}

	defer resp.Body.Close()

	token := resp.Header.Get("SSOTOKEN")

	if resp.StatusCode == 200 {
		byteArr, _ := ioutil.ReadAll(resp.Body)
		jsonStr := string(byteArr)
		if len(jsonStr) > 2 {
			return byteArr, token
		}
	}

	return nil, ""
}

func Post2(ssoToken, toUrl string, param map[string]interface{}) string {

	client := new(http.Client)
	paramToJson, _ := json.Marshal(param)

	req, err2 := http.NewRequest("post", toUrl, strings.NewReader(string(paramToJson)))
	CheckErr(err2)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("SSOTOKEN", ssoToken)

	resp, err := client.Do(req)
	CheckErr(err)

	defer resp.Body.Close()

	byteArr, _ := ioutil.ReadAll(resp.Body)

	jsonStr := string(byteArr)
	if len(byteArr) > 0 {
		return jsonStr
	}

	return ""
}
