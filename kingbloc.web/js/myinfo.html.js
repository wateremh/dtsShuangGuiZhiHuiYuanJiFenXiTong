var sock = null;
var wsuri = "ws://127.0.0.1:2222/ws";
$(function () {
    $.getScript("/api/sso/myinfo?cb=cb");
    // $.get("/api/server/myAccount?" + new Date().getTime(), showAwardInfo);
})


function showAwardInfo(data) {

    var myAccount = data.Account;
    if (!myAccount) {
        return;
    }

    var orders = data.Order;
    if (!orders.length) {
        return;
    }
    //


}


// 修改密码界面
function updatepwd2UI() {
    var htm = [];
    htm.push('<div class="newOrder updatepwd1" >')
    htm.push('<a class="iconfont icon-close icon-close-empty" ></a>')
    htm.push('<h2 > 重置二级密码 </h2>')
    htm.push('<form id="updatePwd2">')
    htm.push('<span ><em class="iconfont icon-mima"></em><input   type="text" maxlength="10" minlength="6"  placeholder="新二级密码" name="pwd2" /></span>')
    htm.push('<a class="submit" >申请重置</a>')
    htm.push('</form>')
    htm.push('</div>')


    function fn() {
        var url = '/api/server/updatepwd2'
        var result = function (res) {
            if (res && res == 2) {
                alert('申请成功！请立即通知你的推荐人进行审批。')
            }
            $('#dialog').html('').hide()
        }

        var params = $("#updatePwd2").serializeObject()

        $.ajax({
            type: "post",
            data: JSON.stringify(params),
            contentType: "application/json", //必须有
            dataType: "json", //表示返回值类型，不必须
            url: url,
            success: result
        });
    }

    $('#dialog').html(htm.join('')).show();
    $('#dialog').find('.submit').click(fn)
    $('#dialog').find('.icon-close').click(closeDialog)
}



// 修改密码界面
function updatepwd1UI() {
    var refId = 09;
    var htm = [];
    htm.push('<div class="newOrder updatepwd1" >')
    htm.push('<a class="iconfont icon-close icon-close-empty" ></a>')
    htm.push('<h2 > 修改一级密码 </h2>')
    htm.push('<form id="updatePwd1">')
    htm.push('<span ><em class="iconfont icon-mima1"></em><input   type="text" placeholder="新密码"  newpwdref="true"  /></span>')
    htm.push('<span ><em class="iconfont icon-mima1"></em><input   type="text" placeholder="再次输入新密码" name="pwd1"  /></span>')
    htm.push('<span ><em class="iconfont icon-mima"></em><input   type="text" placeholder="二级密码" name="pwd2" /></span>')
    htm.push('<a class="submit" >确定</a>')
    htm.push('</form>')
    htm.push('</div>')


    function updatepwd1() {
        var url = '/api/server/updatepwd1'
        var result = function (res) {
            if (res && res == 2) {
                alert('修改成功！')
            }
            $('#dialog').html('').hide()
        }

        var params = $("#updatePwd1").serializeObject()

        $.ajax({
            type: "post",
            data: JSON.stringify(params),
            contentType: "application/json", //必须有
            dataType: "json", //表示返回值类型，不必须
            url: url,
            success: result
        });
    }

    $('#dialog').html(htm.join('')).show();
    $('#dialog').find('.submit').click(updatepwd1)
    $('#dialog').find('.icon-close').click(closeDialog)
}

// 登陆界面
function showNewOrderUI() {
    var refId = 09;
    var htm = [];
    htm.push('<div class="newOrder" >')
    htm.push('<a class="iconfont icon-close icon-close-empty" ></a>')
    htm.push('<h2 > 报单 </h2>')
    htm.push('<form id="login">')
    htm.push('<span ><em class="iconfont icon-icon"></em><input   type="text" placeholder="手机" name="mobile" value="18620131415" /></span>')
    htm.push('<span ><em class="iconfont icon-mima"></em><select  placeholder="金额" name="price" >')
    htm.push('<option value="3000" selected >3000 快</option>')
    htm.push('<option value="10000" >10000 快</option>')
    htm.push('<option value="20000" >20000 快</option>')
    htm.push('<option value="30000" >30000 快</option>')
    htm.push('</select></span>')
    htm.push('<span ><em class="iconfont icon-mima"></em><input   type="text" placeholder="推荐人手机" name="introducerMob"   value="000000" /></span>')
    htm.push('<a class="submit" >确定</a>')
    htm.push('</form>')
    htm.push('</div>')

    $('#dialog').html(htm.join('')).show();
    $('#dialog').find('.submit').click(newOrderFn)
    $('#dialog').find('.icon-close').click(closeDialog)
}


function showCalcAwar() {
    var url = '/api/server/calcAward'
    var result = function (res) {
        var btn = $("#calcAward");
        btn.html("计算中...");
        btn.removeClass('on').addClass('off')
        btn.removeAttr('onclick')
    }

    $.ajax({
        type: "get",
        url: url,
        success: result
    });
}


function newOrderFn() {
    var url = '/api/server/newOrder'
    var data = $("#login").serializeObject()
    if (!(/^1(3|4|5|7|8)\d{9}$/.test(data.mobile))) {
        alert("手机号码有误，请重填");
        return;
    }
    if (!(/^1(3|4|5|7|8)\d{9}$/.test(data.introducerMob))) {
        alert("手机号码有误，请重填");
        return;
    }


    var newOrderResult = function (res) {
        if (res == 'ok') {
            alert("报单成功！")
            $("#dialog").html("")
        }
    }

    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(data),
        contentType: 'application/json',
        success: newOrderResult
    });


}


