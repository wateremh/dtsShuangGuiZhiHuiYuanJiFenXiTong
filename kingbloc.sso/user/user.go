package user

import (
	"strconv"
	"time"

	"kingbloc.util/util"
)

// SSO User
type User struct {
	Id       int64     `json:"id"`
	Username string    `json:"username"`
	Password string    `json:"-"`
	Alias    string    `json:"alias"`
	Mobile   string    `json:"moblie"`
	Alipay   string    `json:alipay`
	Wechat   string    `json:wechat`
	QQ       int64     `json:"qq,string"` //QQ
	Email    string    `json:"email"`
	City     string    `json:"city"`
	Address  string    `json:"addr"`
	Sex      int       `json:"sex,string"` //性别
	Identity string    `json:"-"`
	Create   time.Time `json:"-"`           //注册日期
	Last     time.Time `json:"last,string"` //上次登录日期
}

func NewUser() User {
	u := User{}
	return u
}

func (cate *User) Add() (*User, int64, error) {
	refId, err := util.Eng.Insert(cate)
	if err != nil {
		// id is identity
		return nil, 0, err
	}
	return cate, refId, nil
}

func (cate *User) DelAll() {
	sql := "DELETE FROM user;"
	sql1 := "ALTER TABLE user AUTO_INCREMENT=1;"
	_, err := util.Eng.Exec(sql)
	util.CheckErr(err)
	_, err = util.Eng.Exec(sql1)
	util.CheckErr(err)
}

func (data *User) Del(id int64) error {

	data.Id = id
	_, err := util.Eng.Delete(data)
	return err
}

func (cate *User) ById(refId string) *User {

	id, err := strconv.ParseInt(refId, 10, 64)
	if id == 0 {
		return nil
	}
	util.CheckErr(err)
	_, err = util.Eng.Id(id).Get(cate)
	util.CheckErr(err)
	return cate
}

func (data *User) Edit() error {
	if data.Id == 0 {
		return nil
	}

	_, err := util.Eng.Id(data.Id).Update(data)
	util.CheckErr(err)
	return nil
}

func (da *User) UpdateByCol(field string) {
	_, err := util.Eng.Id(da.Id).Cols(field).Update(da)
	util.CheckErr(err)

}

func (data *User) Update(dataRef User) error {
	if dataRef.Id == 0 {
		return nil
	}

	_, err := util.Eng.Id(dataRef.Id).Update(dataRef)
	util.CheckErr(err)
	return nil
}

func (cate *User) ByMobile() *User {
	sql := "select * from user as U where U.mobile =?  limit 1"
	slice, _ := util.Eng.Query(sql, cate.Mobile)
	if len(slice) == 0 {
		return nil
	}
	cate = Compound(slice, 0)
	return cate
}

func (user *User) ByEmail() *User {
	sql := "select * from user as U where U.email  =? limit 1"
	Slice, _ := util.Eng.Query(sql, user.Email)
	if len(Slice) == 0 {
		return nil
	}
	user = Compound(Slice, 0)
	return user
}

func (user *User) ByUsername() *User {
	sql := "select * from user as U where U.username =?  limit 1"
	Slice, _ := util.Eng.Query(sql, user.Username)
	if len(Slice) == 0 {
		return nil
	}
	user = Compound(Slice, 0)
	return user
}

func Compound(resultsSlice []map[string][]byte, no int) *User {
	user := new(User)
	var err error

	Slice := resultsSlice[no]
	for k, v := range Slice {
		val := string(v)
		switch k {
		case "id":
			user.Id, err = strconv.ParseInt(val, 10, 64)
		case "username":
			user.Username = val
		case "password":
			user.Password = val
		case "alias":
			user.Alias = val
		case "mobile":
			user.Mobile = val
		case "alipay":
			user.Alipay = val
		case "wechat":
			user.Wechat = val
		case "qq":
			user.QQ, err = strconv.ParseInt(val, 10, 64)
		case "email":
			user.Email = val
		case "city":
			user.City = val
		case "address":
			user.Address = val
		case "sex":
			user.Sex, err = strconv.Atoi(val)
		case "identity":
			user.Identity = val
		case "create":
			user.Create, err = time.Parse(util.TimeFormat, val)
		case "last":
			user.Last = time.Now()
		}
		util.CheckErr(err)
	}

	return user
}
