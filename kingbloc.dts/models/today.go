package model

import "time"

// 每天业绩
type Today struct {
	OrderId   int64 `json:"orderid"`
	AccountId int64 `json:"accountid"`
}

func NewToday(orderId, accountId int64) (today *Today) {
	today = new(Today)
	today.OrderId = orderId
	today.AccountId = accountId
	return
}

func (t *Today) Add() {
	MysqlEngine.InsertOne(t)
}
func (t *Today) All() []*Today {
	ts := make([]*Today, 0)
	MysqlEngine.AllCols().Find(&ts)
	return ts
}
func (t *Today) Del() {
	MysqlEngine.Delete(t)
}

// 每月业绩
type Month struct {
	CreateAt  time.Time // 计算时间
	DayOrders string    `json:"dayorderids" xorm:"varchar(9000)"`
}

func NewMonth(orderId, accountId int64) (today *Month) {
	today = new(Month)
	return
}

func (t *Month) Add() {
	MysqlEngine.InsertOne(t)
}
func (t *Month) All() {
	ms := make([]*Month, 0)
	MysqlEngine.AllCols().Find(&ms)
}
func (t *Month) Del() {
	MysqlEngine.Delete(t)
}
