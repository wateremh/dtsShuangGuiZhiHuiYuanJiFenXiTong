package controller

import (
	"kingbloc.dts/services"
	"kingbloc.util/util"
	"net/http"
	"strings"
)

// 授予报单权限
func GrantNewOrder(rep http.ResponseWriter, req *http.Request) {

	user := validateUserInfo(req)

	if user == nil {
		rep.WriteHeader(404)
		return
	}

	myAccount := service.FindAccountBySSOId(user.Id)

	if myAccount == nil || myAccount.Id == 0 {
		rep.WriteHeader(404)
		return
	}

	if strings.ToLower(req.Method) != "get" {
		rep.WriteHeader(404)
		return
	}

	mob := req.URL.Query().Get("mob")
	if mob == "" {
		rep.WriteHeader(404)
		return
	}

	if !util.RegexpMobile.MatchString(mob) {
		rep.WriteHeader(404)
		return
	}

	// 账户类型
	if myAccount.Types != 3 {
		rep.WriteHeader(404)
		return
	}

	result := map[string]interface{}{}

	account := service.FindAccountByMob(mob)

	if account == nil || account.Id == 0 {
		result["status"] = 0
		result["msg"] = "账户不存在"

		util.WriteJSON(rep, result)
		return
	}

	account.Types = 1
	account.UpdateCols("types")
	//
	result["status"] = 10
	result["msg"] = "授权成功！"

	util.WriteJSON(rep, result)

}
